variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-west-1"
}


variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

