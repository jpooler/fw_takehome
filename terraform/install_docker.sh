#!/bin/bash
# FROM https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04
sudo apt update
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install -y docker-ce
sudo systemctl status docker
sudo usermod -aG docker ubuntu
docker network create --driver bridge ruby-net
docker run --name rails-postgres --restart unless-stopped -p 5432:5432 --network ruby-net -e POSTGRES_PASSWORD=mysecretpassword -d postgres
sleep 20
docker run --name fw-takehome --restart unless-stopped --network ruby-net -p 3000:3000 -d  registry.gitlab.com/jpooler/fw_takehome 
