## fw Takehome Challenge

Assumptions and Notes:

1. I'm assuming end-user has a terminal app, docker, terraform, aws credentials, and an ec2 ssh keypair installed on mac laptop, some initial code was added to test for these but took far too long in subsequent runs to leave in place. Additional code could be added to test for OS, but for the purposes of this exercise skipped. 
2. Given the access level, assumed scaling capability was less of a priority based on incurred costs.
3. I chose to use Docker as it helps bypass needing more complicated configuration management systems, as well as enabling local/remote development setup.
4. I chose to host the source control in gitlab as it provides a container registry and other ci/cd functionality that could help should it be required, avoiding the need to setup ECR or another container registry.
5. I chose Rails as my most recent experiences have been with it at vmware (ecs/fargate at chewy). 
6. I chose to re-use network generation code from a previous takehome test, as to save time on the overall test, given that in theory we could have scoped it to a much much smaller range and public-only subnets.
6. I chose to lock it down to my own IP to limit the "blast radius."
7. I chose to manually build/push the container during testing, it could easily be incorporated into ci/cd to build on push to a branch
8. I hardcoded a bunch of things I would normally not, in lieu of time/speed, (postgres password), and could have definitely added more testing/failsafe/retry logic to the userdata script as well as the entrypoint script with more time. I also could have used docker compose or created actual system-level services instead of running docker in daemon mode.

