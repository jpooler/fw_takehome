#!/bin/bash

#if ! command -v terraform &> /dev/null
#then
#    echo "Terraform not installed!"
#    exit
#fi
#
#if ! command -v docker &> /dev/null
#then
#    echo "docker not installed!"
#    exit
#fi

# Build Container * done manually, but would in theory be part of ci/cd system
# docker login registry.gitlab.com
# docker build -t registry.gitlab.com/jpooler/fw_takehome .
# docker push registry.gitlab.com/jpooler/fw_takehome




#cd terraform && terraform init && terraform plan
cd terraform && terraform init && terraform apply -auto-approve && terraform output instance_ip_addr
